﻿using System;

using homework4.Models;

namespace homework4.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public Item Item { get; set; }

        public String imgUrl { get; set; }
        public ItemDetailViewModel(Item item = null)
        {
            Title = item?.name;
            Item = item;
        }
    }
}
