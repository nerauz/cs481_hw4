﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using homework4.Models;
using Newtonsoft.Json;

namespace homework4.Services
{
    public class MockDataStore : IDataStore<Item>
    {
        public List<Item> items;

        public MockDataStore()
        {
            items = new List<Item>();

            items.Add(new Item { id = "1", name = "bulbasaur" });
            items.Add(new Item { id = "2", name = "ivysaur" });
            items.Add(new Item { id = "3", name = "venusaur" });
            items.Add(new Item { id = "4", name = "charmander" });
            items.Add(new Item { id = "5", name = "charmeleon" });
            items.Add(new Item { id = "6", name = "charizard" });
            items.Add(new Item { id = "7", name = "squirtle" });
            items.Add(new Item { id = "8", name = "wartortle" });
            items.Add(new Item { id = "9", name = "blastoise" });
            items.Add(new Item { id = "10", name = "caterpie" });
            items.Add(new Item { id = "11", name = "metapod" });
            items.Add(new Item { id = "12", name = "butterfree" });
            items.Add(new Item { id = "13", name = "weedle" });
            items.Add(new Item { id = "14", name = "kakuna" });
            items.Add(new Item { id = "15", name = "beedrill" });
            items.Add(new Item { id = "16", name = "pidgey" });
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            var oldItem = items.Where((Item arg) => arg.name == item.name).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string name)
        {
            var oldItem = items.Where((Item arg) => arg.name == name).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Item> GetItemAsync(string name)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.name == name));
        }

        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }
    }
}