﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using homework4.Models;
using homework4.ViewModels;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace homework4.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class ItemDetailPage : ContentPage
    {
        ItemDetailViewModel viewModel;

        public ItemDetailPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();

            String url = String.Format("https://pokeres.bastionbot.org/images/pokemon/{0}.png", viewModel.Item.id);
            
            viewModel.imgUrl = url;

            BindingContext = this.viewModel = viewModel;
        }

    }

}